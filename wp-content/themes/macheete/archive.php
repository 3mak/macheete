<?php get_header(); ?>
<section class="archive">
	<div class="row">
		<!-- Row for main content area -->
		<div class="medium-7 columns" role="main">

			<?php if(have_posts()) : the_post() ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('blog-preview big'); ?>>
					<div class="row">
						<div class="medium-12 show-for-medium-up columns grayscale">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
						</div>
						<div class="small-12 columns">
							<header>
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php foundationpress_entry_meta(); ?>

							</header>
							<div class="entry-content">
								<?php the_excerpt(); ?>
							</div>
							<footer>
								<?php $tag = get_the_tags();
								if ( ! $tag ) {
								} else { ?><p><?php the_tags(); ?></p><?php } ?>
							</footer>
						</div>
					</div>
				</article>
			<?php endif; ?>

			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'blog-content', get_post_format() ); ?>
				<?php endwhile; ?>

			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; // end have_posts() check ?>

			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php if ( function_exists( 'foundationpress_pagination' ) ) {
				foundationpress_pagination();
			} else if ( is_paged() ) { ?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts',
							'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;',
							'foundationpress' ) ); ?></div>
				</nav>
			<?php } ?>
		</div>

		<div class="medium-4 medium-offset-1 columns">
			<aside>
				<?php  dynamic_sidebar('sidebar-widgets'); ?>
				<?php get_template_part('parts/sm-icons')?>
			</aside>

		</div>


	</div>
</section>
<?php get_footer( 'contact' ); ?>
