<?php get_header(); ?>
<div class="row">
	<!-- Row for main content area -->
	<div class="small-12 large-12 columns" role="main">
		<?php if ( have_posts() ) : ?>
			<?php
			$category_slug = "pl-category";
			$block_grid    = 4;
			include( locate_template( 'parts/mixitup.php' ) );
			?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; // end have_posts() check ?>

		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php if ( function_exists( 'foundationpress_pagination' ) ) {
			foundationpress_pagination();
		 } ?>

	</div>
</div>
<?php get_footer(); ?>
