<?php get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php the_macheete_big_bg(); ?>

	<article <?php post_class('single') ?> id="post-<?php the_ID(); ?>">
		<div class="row">
			<div class="medium-12 columns">
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<div class="row">
					<div class="medium-4 columns">
						<div class="meta-info">
								<span class="bold"><?php echo __( 'Customer', 'macheete' ) ?>
									:</span> <?php echo get_field( 'kunde' ) ?>
						</div>
						<div class="meta-info">
								<span class="bold"><?php echo __( 'Job', 'macheete' ) ?>
									:</span> <?php echo get_field( 'aufgabe' ) ?>
						</div>
						<div class="meta-info">
							<span class="bold"><?php echo __( 'Web', 'macheete' ) ?>:</span> <a
								href="<?php echo addhttp( get_field( 'web_link' ) ) ?>"
								target="_blank"><?php echo get_field( 'web_link' ) ?></a>
						</div>
					</div>
					<div class="medium-8 columns counted">

						<div class="entry-content">
							<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php if(get_field('medien')): ?>
					<div class="row">
						<div class="small-12 columns media">
							<?php echo the_field( 'medien' ); ?>
						</div>
					</div>
				<?php endif; ?>

				<footer>
					<?php wp_link_pages( array(
						'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
						'after'  => '</p></nav>'
					) ); ?>
					<p><?php the_tags(); ?></p>
				</footer>
			</div>
		</div>

		<?php do_action( 'foundationpress_post_before_comments' ); ?>
		<?php comments_template(); ?>
		<?php do_action( 'foundationpress_post_after_comments' ); ?>
	</article>
<?php endwhile; ?>

<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_footer(); ?>