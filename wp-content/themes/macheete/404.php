<?php get_header(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="error-bg">
				<div class="border">
					<div class="alt-font big">404</div>
					<p class="bottom"><?php echo sprintf(__('Miauuuuuuu, this page does not exist! Don’t worry. Click <a href="%s">here</a> to return to homepage. But before you exit, caressing the Tiger a bit with your mouse <3','macheete'), get_home_url()); ?></p>
				</div>
			</div>
		</article>

<?php get_footer('light'); ?>
