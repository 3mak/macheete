<?php get_header(); ?>


<?php while ( have_posts() ) : the_post(); ?>
	<?php the_macheete_big_bg(); ?>

	<?php do_action( 'foundationpress_before_content' ); ?>

	<article <?php post_class('single') ?> id="post-<?php the_ID(); ?>">
		<div class="row">
			<div class="small-12 columns">
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>

				<div class="entry-content counted">
					<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
					<?php the_content(); ?>
				</div>

				<?php if ( get_field( 'medien' ) ): ?>
					<div class="row">
						<div class="small-12 columns">
							<div class="media">
								<?php the_field( 'medien' ); ?>
							</div>
						</div>
					</div>

				<?php endif; ?>

				<?php if ( get_field( 'pressebilder') ): ?>
					<div class="row">
						<div class="small-12 columns">
							<div class="presse">
								<h2><?php echo __( 'Press Images', 'macheete' ); ?></h2>
								<?php the_field( 'pressebilder' ); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php $pressemitteilungen = get_field_object( 'field_559ce7b937b8a', get_the_ID()); ?>
				<?php if ( is_array( $pressemitteilungen['value']) ): ?>
					<h2><?php echo __( 'Press Releases ', 'macheete' ); ?></h2>
					<?php foreach ( $pressemitteilungen['value'] as $article ): ?>
						<?php $date = new DateTime( $article->post_date ); ?>
						<div><?php echo $date->format( 'd.m.Y' ); ?> | <a
								href="<?php echo get_permalink( $article->ID ) ?>"><?php echo $article->post_title; ?></a>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if ( get_field( 'ansprechpartner' ) ): ?>
					<h2><?php echo __( 'Contact person', 'macheete' ); ?></h2>
					<?php the_field( 'ansprechpartner' ); ?>
				<?php endif; ?>
			</div>
		</div>

		<footer>
			<?php wp_link_pages( array(
				'before' => '<nav id="page-nav"><p>' . __( 'Pages:',
						'foundationpress' ),
				'after'  => '</p></nav>'
			) ); ?>
			<p><?php the_tags(); ?></p>
		</footer>

		<?php do_action( 'foundationpress_post_before_comments' ); ?>
		<?php comments_template(); ?>
		<?php do_action( 'foundationpress_post_after_comments' ); ?>
	</article>
<?php endwhile; ?>

<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_footer(); ?>
