<?php
/**
Template Name: Archive
 */

?>
<?php get_header( 'small' ); ?>
<section class="archive">
    <div class="row">
        <!-- Row for main content area -->
        <div class="medium-7 columns" role="main">
            <?php $paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
			$args = array( 'post_type' => 'post', 'posts_per_page' => get_option( 'posts_per_page' ), 'paged' => $paged );
			$wp_query = new WP_Query( $args );
			?>

            <?php if ( have_posts() ) : ?>
                <?php $featured_displayed = false; ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php if ( false === $featured_displayed ) : ?>
                        <?php get_template_part( 'parts/blog-content-big', get_post_format() ); ?>
                        <?php $featured_displayed = true; ?>
                    <?php else : ?>
                        <?php get_template_part( 'blog-content', get_post_format() ); ?>
                    <?php endif; ?>
                <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>

            <?php endif; ?>

            <?php /* Display navigation to next/previous pages when applicable */ ?>
            <?php if ( function_exists( 'foundationpress_pagination' ) ) {
				foundationpress_pagination();
			} ?>
        </div>
        <div class="medium-4 medium-offset-1 columns">
            <aside>
                <?php dynamic_sidebar( 'sidebar-widgets' ); ?>
                <?php get_template_part('parts/sm-icons')?>
            </aside>

        </div>


    </div>
</section>
<?php wp_reset_postdata(); ?>
<?php get_footer( 'contact' ); ?>
