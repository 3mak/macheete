<?php
/**
 * Template Name: Contact
 */

?>
<?php get_header( 'small' ); ?>
<div id="map" class="hide-for-small"></div>
<div class="row">
	<div class="small-12 columns" role="main">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( have_posts() ) : ?>

			<?php do_action( 'foundationpress_before_content' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<header>
					<h1><?php the_title(); ?></h1>
				</header>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

			<?php do_action( 'foundationpress_before_pagination' ); ?>

		<?php endif; ?>
		</article>

		<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
</div>
<?php get_footer( 'contact' ); ?>
