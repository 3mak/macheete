<?php
/*
Template Name: Full Width
*/
get_header(); ?>
<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class( 'no-margin' ) ?> id="post-<?php the_ID(); ?>">

		<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<?php do_action( 'foundationpress_page_before_comments' ); ?>
		<?php comments_template(); ?>
		<?php do_action( 'foundationpress_page_after_comments' ); ?>
	</article>
<?php endwhile; // End the loop ?>
<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_footer( 'light' ); ?>
