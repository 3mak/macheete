<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog-preview'); ?>>
	<div class="row">
		<div class="medium-4 show-for-medium-up columns grayscale">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
		</div>
		<div class="medium-8 columns">
			<header>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php foundationpress_entry_meta(); ?>

			</header>
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div>
			<footer>
				<?php $tag = get_the_tags();
				if ( ! $tag ) {
				} else { ?><p><?php the_tags(); ?></p><?php } ?>
			</footer>
		</div>
	</div>
</article>
