<?php get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php the_macheete_big_bg(); ?>

	<?php do_action( 'foundationpress_before_content' ); ?>
	<article <?php post_class( 'single' ) ?> id="post-<?php the_ID(); ?>">
		<div class="row">
			<div class="small-12 columns">
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>

				<div class="entry-content">
					<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
					<?php the_content(); ?>
				</div>

				<footer class="text-center">
					<?php //next_post_link( '%link', __( '&larr; Previous', ',macheete' ), true); ?>
					<?php previous_post_link( '%link', __( 'Next &rarr;', 'macheete' ), true); ?>
				</footer>
			</div>
		</div>

		<?php do_action( 'foundationpress_post_before_comments' ); ?>
		<?php comments_template(); ?>
		<?php do_action( 'foundationpress_post_after_comments' ); ?>
	</article>
<?php endwhile; ?>

<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_footer(); ?>
