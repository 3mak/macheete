<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

$languages = icl_get_languages( 'skip_missing=0&orderBy=active' );
?>

<ul class="language-switcher">
	<?php foreach ( $languages as $language ) : ?>
		<?php if ($language['active'] == 1 ) : ?>
			<?php $active = $language; ?>
		<?php else : ?>
			<li><a href="<?php echo $language['url']?>"><?php echo $language['language_code']?></a></li>
		<?php endif; ?>
	<?php endforeach; ?>
	<li class="active"><a href="<?php echo $active['url']?>"><?php echo $active['language_code']?></a></li>
</ul>
