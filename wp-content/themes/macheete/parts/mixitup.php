<?php $block_grid = ( ! isset( $block_grid ) ) ? 4 : $block_grid ?>
<?php $hidden_categories = array( 'top-cases' ); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class( 'mixitup' ); ?>>

	<ul class="inline-list">
		<li><a href="#" onclick="return false;" class="filter" data-filter=".all"><?php _e( 'All', 'macheete' ); ?></a></li>
		<?php foreach ( get_terms( $category_slug ) as $category ):   ?>
			<?php if ( ! in_array( $category->slug, $hidden_categories ) ): ?>
				<li class="<?php echo $category->slug; ?>">
					<a href="#" onclick="return false" class="filter"
					   data-filter=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>

	<?php /* Mix Blocks */ ?>
	<div id="Container">
		<ul class="small-block-grid-1 medium-block-grid-<?php echo $block_grid; ?>">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$casesCategories = get_the_terms( get_the_ID(), $category_slug );
				$cat_slugs       = null;
				if ( is_array( $casesCategories ) ) {
					foreach ( $casesCategories as $category ) {
						$cat_slugs[] = $category->slug;
					}
				}
				?>
				<?php if(0 === wp_get_post_parent_id(get_the_ID())): ?>
				<li class="mix all <?php echo implode( ' ', $cat_slugs ) ?>">
					<?php if ( has_post_thumbnail() ): ?>
						<?php
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' );
						$url   = $thumb['0'];
						?>
					<?php endif; ?>
					<article role="article">
						<a class="case" href="<?php the_permalink() ?>">
							<div class="zoom grayscale" style="background-image: url('<?php echo $url; ?>');">
							</div>
							<div class="intro">
								<h1 class="title"><?php the_title(); ?></h1>
								<?php if ( get_field( 'aufgabe' ) ) : ?>
									<span class="desc"><?php echo the_field( 'aufgabe' )?></span>
								<?php endif; ?>
							</div>
						</a>
					</article>
				</li>
				<?php endif; ?>
			<?php endwhile; ?>
		</ul>
	</div>
</div>
