<article class="row widget">
    <div class="small-12 columns">
        <div class="wrapper">
            <hr/>
            <ul class="sm-icons">
                <li><a class="sm-icon facebook" target="_blank" href="https://www.facebook.com/macheete"></a></li>
                <li><a class="sm-icon twitter" target="_blank" href="https://twitter.com/macheete_berlin"></a></li>
                <li><a class="sm-icon instagram" target="_blank" href="https://instagram.com/macheete/"></a></li>
                <li><a class="sm-icon pinterest" target="_blank" href="https://pinterest.com/macheete/"></a></li>
                <li><a class="sm-icon xing" target="_blank" href="https://www.xing.com/companies/macheete"></a></li>
            </ul>
        </div>
    </div>
</article>