<div class="sticky-nav">
	<a href="<?php echo get_home_url(); ?>">
		<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/macheete_logo_web.png" alt="MACHEETE"/>
	</a>

	<div class="row">
		<div class="small-11 columns">
			<?php foundationpress_sticky_nav(); ?>
		</div>
		<div class="small-1 columns">
			<?php get_template_part('parts/language-switcher'); ?>
		</div>
	</div>
</div>

