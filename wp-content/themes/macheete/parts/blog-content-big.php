<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-preview big'); ?>>
<div class="row">
    <div class="medium-12 show-for-medium-up columns grayscale">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
    </div>
    <div class="small-12 columns">
        <header>
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php foundationpress_entry_meta(); ?>

        </header>
        <div class="entry-content">
            <?php the_excerpt(); ?>
        </div>
        <footer>
            <?php $tag = get_the_tags();
            if ( ! $tag ) {
            } else { ?><p><?php the_tags(); ?></p><?php } ?>
        </footer>
    </div>
</div>
</article>