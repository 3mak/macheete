<?php

class Macheete_Post_Wall extends WP_Widget {

// constructor
	function __construct() {
		$widget_ops = array(
			'classname'   => 'macheete_post_wall_widget',
			'description' => __( 'Post Wall', 'macheete' )
		);
		parent::__construct( false, $name = __( 'Macheete: Post Wall', 'macheete' ), $widget_ops );
		$this->alt_option_name = 'macheete_post_wall';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	// widget form creation
	function form( $instance ) {

		$title    = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$category = isset( $instance['category'] ) ? esc_attr( $instance['category'] ) : '';
		?>

		<!-- Widget Title -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'macheete' ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $title ?>"/>
		</p>

		<!-- Category Select Menu -->
		<p>
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php echo __( 'Category:', 'macheete' ) ?></label>
			<select id="<?php echo $this->get_field_id( 'category' ); ?>"
			        name="<?php echo $this->get_field_name( 'category' ); ?>" class="widefat" style="width:100%;">
				<?php foreach ( get_terms( 'case-category', 'parent=0&hide_empty=0' ) as $term ) { ?>
					<option <?php selected( $instance['category'], $term->slug ); ?>
						value="<?php echo $term->slug ?>"><?php echo $term->name; ?></option>
				<?php } ?>
			</select>
		</p>
	<?php
	}

	// update widget
	function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['category'] = strip_tags( $new_instance['category'] );
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['macheete_post_wall'] ) ) {
			delete_option( 'macheete_post_wall' );
		}

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( 'macheete_post_wall', 'widget' );
	}

	// display widget
	function widget( $args, $instance ) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'macheete_post_wall', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];

			return;
		}

		ob_start();
		extract( $args );

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Contact info', 'macheete' );

		echo $before_widget;

		?>
		<?php if ( $title ): ?>
			<h3 class="title"><?php echo $title; ?></h3>
		<?php endif; ?>

		<?php
		$args       = array(
			'case-category' => $instance['category'],
			'post_type'     => 'referenzen',
			'posts_per_page' => 6
		);
		$page_query = new WP_Query( $args );
		$columns    = ( $page_query->post_count <= 3 ) ? $page_query->post_count : 3;
		?>

		<ul class="small-block-grid-1 medium-block-grid-<?php echo $columns; ?>">
			<?php while ( $page_query->have_posts() ) : $page_query->the_post(); ?>
				<?php if ( has_post_thumbnail() ) : ?>
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),
						'large' );
					$url         = $thumb['0']; ?>
				<?php endif; ?>
				<li>
					<a class="case" href="<?php the_permalink() ?>">
						<div class="zoom grayscale" style="background-image: url('<?php echo $url; ?>');"></div>
						<div class="intro">
							<span class="title"><?php the_title(); ?></span>
							<?php if(get_field('aufgabe')): ?>
								<span class="desc"><?php echo the_field('aufgabe')?></span>
							<?php endif; ?>
						</div>
					</a>
				</li>
			<?php endwhile; ?>
		</ul>

		<?php echo $after_widget;


		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'macheete_post_wall', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

}	