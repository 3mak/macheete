<?php

/**
 * Created by PhpStorm.
 * User: emak
 * Date: 18.05.15
 * Time: 09:49
 */
class Macheete_Leistungen extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'macheete_leistungen', // Base ID
			__( 'Macheete: Services', 'macheete' ), // Name
			array( 'description' => __( 'Show your Services', 'macheete' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']; ?>
		<h3><?php echo $instance['title']; ?></h3>
		<ul class="tabs" data-tab>
			<?php for ( $i = 1; $i <= 5; $i ++ ): ?>
				<?php $selector = preg_replace( '/[^A-Za-z0-9\-]/', '', (strtolower( str_replace( ' ', ' ', $instance[ 'title_' . $i ] ) ) ) ); ?>
				<li class="tab-title <?php echo ( $i == 1 ) ? 'active' : '' ?>"><a href="#<?php echo $selector; ?>"
				                                                                   class="serviceoverview-<?php echo $i; ?>"><?php echo $instance[ 'title_' . $i ]; ?></a>
				</li>
			<?php endfor; ?>
		</ul>
		<div class="tabs-content">
			<?php for ( $i = 1; $i <= 5; $i ++ ): ?>
				<?php $selector = preg_replace( '/[^A-Za-z0-9\-]/', '', (strtolower( str_replace( ' ', ' ', $instance[ 'title_' . $i ] ) ) ) ); ?>

				<div class="content <?php echo ( $i == 1 ) ? 'active' : '' ?>" id="<?php echo $selector; ?>">
					<div class="row">
						<div class="medium-4 columns quote"><?php echo $instance[ 'quote_' . $i ]; ?></div>
						<div class="medium-8 columns counted"><?php echo $instance[ 'text_' . $i ]; ?></div>
					</div>
				</div>
			<?php endfor; ?>
		</div>

		<?php echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Services', 'macheete' );
		$button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : __( 'More about our services',
			'macheete' );
		?>

		<!-- Title-->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
		</p>


		<?php for ( $i = 1; $i <= 5; $i ++ ): ?>
			<?php $field_name = sprintf( 'text_%d', $i ); ?>
			<?php $quote_name = sprintf( 'quote_%d', $i ); ?>
			<?php $title_name = sprintf( 'title_%d', $i ); ?>
			<?php $text = isset( $instance[ $field_name ] ) ? $instance[ $field_name ] : __( 'Text ' . $i,
				'macheete' ); ?>
			<?php $quote = isset( $instance[ $quote_name ] ) ? $instance[ $quote_name ] : __( 'Quote ' . $i,
				'macheete' ); ?>
			<?php $title = isset( $instance[ $title_name ] ) ? $instance[ $title_name ] : __( 'Title ' . $i,
				'macheete' ); ?>

			<h3><?php echo __( "Feld " . $i ); ?></h3>
			<p>

				<label for="<?php echo $this->get_field_id( $field_name ); ?>"><?php _e( 'Title', 'macheete' ); ?></label>
				<input class="widefat" type="text" id="<?php echo $this->get_field_id( $title_name ); ?>"
				       name="<?php echo $this->get_field_name( $title_name ); ?>"
				       value="<?php echo esc_attr( $title ); ?>">
			</p>
			<p>

				<label for="<?php echo $this->get_field_id( $field_name ); ?>"><?php _e( 'Text', 'macheete' ); ?></label>
				<textarea class="widefat" id="<?php echo $this->get_field_id( $field_name ); ?>"
				          name="<?php echo $this->get_field_name( $field_name ); ?>"><?php echo esc_attr( $text ); ?></textarea>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( $quote_name ); ?>"><?php _e( 'Quote', 'macheete' ); ?></label>
				<textarea class="widefat" id="<?php echo $this->get_field_id( $quote_name ); ?>"
				          name="<?php echo $this->get_field_name( $quote_name ); ?>"><?php echo esc_attr( $quote ); ?></textarea>
			</p>
		<?php endfor; ?>

		<!-- Button Text-->
		<p>
			<label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( 'Button Text:', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'button_text' ); ?>"
			       name="<?php echo $this->get_field_name( 'button_text' ); ?>" type="text"
			       value="<?php echo esc_attr( $button_text ); ?>">
		</p>
	<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $new_instance;

		return $instance;
	}

} 