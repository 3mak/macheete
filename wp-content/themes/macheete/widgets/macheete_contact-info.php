<?php

class Macheete_Contact_Info extends WP_Widget {

// constructor
	function macheete_contact_info() {
		$widget_ops = array(
			'classname'   => 'macheete_contact_info_widget',
			'description' => __( 'Display your contact info', 'macheete' )
		);
		parent::WP_Widget( false, $name = __( 'Macheete: Contact info', 'macheete' ), $widget_ops );
		$this->alt_option_name = 'macheete_contact_info';

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	// widget form creation
	function form( $instance ) {

		// Check values
		$title   = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$desc    = isset( $instance['desc'] ) ? esc_attr( $instance['desc'] ) : '';
		$address = isset( $instance['address'] ) ? esc_html( $instance['address'] ) : '';
		$phone   = isset( $instance['phone'] ) ? esc_html( $instance['phone'] ) : '';
		$email   = isset( $instance['email'] ) ? esc_html( $instance['email'] ) : '';

		$facebook  = isset( $instance['facebook'] ) ? esc_attr( $instance['facebook'] ) : '';
		$twitter   = isset( $instance['twitter'] ) ? esc_attr( $instance['twitter'] ) : '';
		$instagram = isset( $instance['instagram'] ) ? esc_attr( $instance['instagram'] ) : '';
		$pinterest = isset( $instance['pinterest'] ) ? esc_attr( $instance['pinterest'] ) : '';
		$xing      = isset( $instance['xing'] ) ? esc_attr( $instance['xing'] ) : '';
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'desc' ); ?>"><?php _e( 'Description', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'desc' ); ?>"
			       name="<?php echo $this->get_field_name( 'desc' ); ?>" type="text" value="<?php echo $desc; ?>"/>
		</p>

		<p><label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>"
			       name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo $address; ?>"
			       size="3"/></p>

		<p><label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Phone',
					'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>"
			       name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo $phone; ?>"
			       size="3"/></p>

		<p><label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e( 'Email',
					'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>"
			       name="<?php echo $this->get_field_name( 'email' ); ?>" type="text" value="<?php echo $email; ?>"
			       size="3"/></p>


		<h3>Social Media Links</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'Facebook URL','macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>"
			       name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="url"
			       value="<?php echo $facebook; ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'Twitter URL','macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>"
			       name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="url" value="<?php echo $twitter; ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php _e( 'Instagram URL', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'instagram' ); ?>"
			       name="<?php echo $this->get_field_name( 'instagram' ); ?>" type="url"
			       value="<?php echo $instagram; ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'pinterest' ); ?>"><?php _e( 'Pinterrest URL', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest' ); ?>"
			       name="<?php echo $this->get_field_name( 'pinterest' ); ?>" type="url"
			       value="<?php echo $pinterest; ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'xing' ); ?>"><?php _e( 'XING URL', 'macheete' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'xing' ); ?>"
			       name="<?php echo $this->get_field_name( 'xing' ); ?>" type="url" value="<?php echo $xing; ?>"/>
		</p>
	<?php
	}

	// update widget
	function update( $new_instance, $old_instance ) {
		$instance            = $old_instance;
		$instance['title']   = strip_tags( $new_instance['title'] );
		$instance['address'] = strip_tags( $new_instance['address'] );
		$instance['phone']   = strip_tags( $new_instance['phone'] );
		$instance['desc']    = strip_tags( $new_instance['desc'] );
		$instance['email']   = sanitize_email( $new_instance['email'] );

		$instance['facebook']  = strip_tags( $new_instance['facebook'] );
		$instance['twitter']   = strip_tags( $new_instance['twitter'] );
		$instance['instagram'] = strip_tags( $new_instance['instagram'] );
		$instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
		$instance['xing']      = strip_tags( $new_instance['xing'] );
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['macheete_contact_info'] ) ) {
			delete_option( 'macheete_contact_info' );
		}

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( 'macheete_contact_info', 'widget' );
	}

	// display widget
	function widget( $args, $instance ) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'macheete_contact_info', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];

			return;
		}

		ob_start();
		extract( $args );

		$title   = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Contact info', 'macheete' );
		$title   = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$desc    = isset( $instance['desc'] ) ? esc_html( $instance['desc'] ) : '';
		$address = isset( $instance['address'] ) ? esc_html( $instance['address'] ) : '';
		$phone   = isset( $instance['phone'] ) ? esc_html( $instance['phone'] ) : '';
		$email   = isset( $instance['email'] ) ? esc_html( $instance['email'] ) : '';

		$facebook  = isset( $instance['facebook'] ) ? esc_html( $instance['facebook'] ) : '';
		$twitter   = isset( $instance['twitter'] ) ? esc_html( $instance['twitter'] ) : '';
		$instagram = isset( $instance['instagram'] ) ? esc_html( $instance['instagram'] ) : '';
		$pinterest = isset( $instance['pinterest'] ) ? esc_html( $instance['pinterest'] ) : '';
		$xing      = isset( $instance['xing'] ) ? esc_html( $instance['xing'] ) : '';

		echo $before_widget;

		?>
		<?php if ( $title ): ?>
			<span class="title"><?php echo $title; ?></span>
		<?php endif; ?>

		<?php
		if ( $desc ) {
			echo "<p class='desc'>" . $desc . "</p>";
		}

		?>
		<div class="sm-icons">
			<a class="sm-icon facebook" target="_blank" href="<?php echo $facebook; ?>"></a>
			<a class="sm-icon twitter" target="_blank" href="<?php echo $twitter; ?>"></a>
			<a class="sm-icon instagram" target="_blank" href="<?php echo $instagram; ?>"></a>
			<a class="sm-icon pinterest" target="_blank" href="<?php echo $pinterest; ?>"></a>
			<a class="sm-icon xing" target="_blank" href="<?php echo $xing; ?>"></a>
		</div>
		<?php

		if ( ( $address ) ) {
			echo '<div class="contact address icon">';
			echo '<span>' . $address . '</span>';
			echo '</div>';
		}
		if ( ( $phone ) ) {
			echo '<div class="contact phone icon">';
			echo '<span>' . $phone . '</span>';
			echo '</div>';
		}
		if ( ( $email ) ) {
			echo '<div class="contact email icon">';
			echo '<span><a target="_blank" href="mailto:' . $email . '">' . $email . '</a></span>';
			echo '</div>';
		}

		echo $after_widget;


		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'macheete_contact_info', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

}	