</div>
<footer class="page">
	<div class="row">
		<?php do_action( 'foundationpress_before_footer' ); ?>
		<a href="<?php echo get_home_url() ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/macheete_logo_web.png" alt="MACHEETE" style="width: 100px"/>
		</a>
		<div class="arr">
			<?php echo sprintf(_("&copy; MACHEETE %s | Public Relations, Social Media, Online Marketing"), date('Y', time())) ?>
			<?php foundationpress_footer_nav(); ?>
		</div>
		<?php do_action( 'foundationpress_after_footer' ); ?>
	</div>
</footer>
<a class="exit-off-canvas"></a>

<?php do_action( 'foundationpress_layout_end' ); ?>
</div>
</div>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
