var stickyNavTop = $('.sticky-nav').offset().top;

var stickyNav = function () {
    var scrollTop = $(window).scrollTop();
    if ($('.sticky-nav').hasClass('top') !== true) {
        if (scrollTop > stickyNavTop) {
            $('.sticky-nav').addClass('sticky');
        } else {
            $('.sticky-nav').removeClass('sticky');
        }
    }
};


stickyNav();

$(window).scroll(function () {
    stickyNav();
});