<?php
if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
	function foundationpress_sidebar_widgets() {
		register_sidebar( array(
			'id'            => 'sidebar-widgets',
			'name'          => __( 'Sidebar widgets', 'foundationpress' ),
			'description'   => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
			'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="small-12 columns"><div class="wrapper">',
			'after_widget'  => '</div></div></article>',
			'before_title'  => '<h6>',
			'after_title'   => '</h6>',
		) );

		register_sidebar( array(
			'id'            => 'footer-widgets',
			'name'          => __( 'Footer widgets', 'foundationpress' ),
			'description'   => __( 'Drag widgets to this footer container', 'foundationpress' ),
			'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
			'after_widget'  => '</article>',
			'before_title'  => '<h6>',
			'after_title'   => '</h6>',
		) );

		register_sidebar( array(
			'id'            => 'footer-contact-widgets',
			'name'          => __( 'Footer Contact widgets', 'foundationpress' ),
			'description'   => __( 'Drag widgets to this footer container', 'foundationpress' ),
			'before_widget' => '<div class="center"><article id="%1$s" class="row widget %2$s"><div class="small-12 columns">',
			'after_widget'  => '</div></article></div>',
		) );
	}

	add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;
?>
