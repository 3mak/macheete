<?php

/**
 * Register Menus
 * http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 */
register_nav_menus( array(
	'sticky-nav'        => 'Sticky Top Navigation',
	'mobile-off-canvas' => 'Mobile',
	'footer-nav' => 'Footer Navigation'
) );


/**
 * Sticky Nav
 */
if ( ! function_exists( 'foundationpress_sticky_nav' ) ) {
	function foundationpress_sticky_nav() {
		wp_nav_menu( array(
			'container'       => false,                           // remove nav container
			'container_class' => '',                        // class of container
			'menu'            => '',                                   // menu name
			'menu_class'      => '',                             // adding custom nav class
			'theme_location'  => 'sticky-nav',                // where it's located in the theme
			'before'          => '',                                 // before each link <a>
			'after'           => '',                                  // after each link </a>
			'link_before'     => '',                            // before each link text
			'link_after'      => '',                             // after each link text
			'depth'           => 5,                                   // limit the depth of the nav
			'fallback_cb'     => false,                         // fallback function (see below)
			'walker'          => new Foundationpress_Top_Bar_Walker()
		) );
	}
}

/**
 * Footer Nav
 */
if ( ! function_exists( 'foundationpress_footer_nav' ) ) {
	function foundationpress_footer_nav() {
		wp_nav_menu( array(
			'container'       => false,                           // remove nav container
			'container_class' => '',                        // class of container
			'menu'            => '',                                   // menu name
			'menu_class'      => 'footer-nav',                             // adding custom nav class
			'theme_location'  => 'footer-nav',                // where it's located in the theme
			'before'          => '',                                 // before each link <a>
			'after'           => '',                                  // after each link </a>
			'link_before'     => '',                            // before each link text
			'link_after'      => '',                             // after each link text
			'depth'           => 5,                                   // limit the depth of the nav
			'fallback_cb'     => false,                         // fallback function (see below)
			'walker'          => new Foundationpress_Top_Bar_Walker()
		) );
	}
}

/**
 * Mobile off-canvas
 */
if ( ! function_exists( 'foundationpress_mobile_off_canvas' ) ) {
	function foundationpress_mobile_off_canvas() {
		wp_nav_menu( array(
			'container'       => false,                           // remove nav container
			'container_class' => '',                        // class of container
			'menu'            => '',                                   // menu name
			'menu_class'      => 'off-canvas-list',              // adding custom nav class
			'theme_location'  => 'mobile-off-canvas',        // where it's located in the theme
			'before'          => '',                                 // before each link <a>
			'after'           => '',                                  // after each link </a>
			'link_before'     => '',                            // before each link text
			'link_after'      => '',                             // after each link text
			'depth'           => 5,                                   // limit the depth of the nav
			'fallback_cb'     => false,                         // fallback function (see below)
			'walker'          => new Foundationpress_Offcanvas_Walker()
		) );
	}
}

/**
 * Add support for buttons in the top-bar menu:
 * 1) In WordPress admin, go to Apperance -> Menus.
 * 2) Click 'Screen Options' from the top panel and enable 'CSS CLasses' and 'Link Relationship (XFN)'
 * 3) On your menu item, type 'has-form' in the CSS-classes field. Type 'button' in the XFN field
 * 4) Save Menu. Your menu item will now appear as a button in your top-menu
 */
if ( ! function_exists( 'foundationpress_add_menuclass' ) ) {
	function foundationpress_add_menuclass( $ulclass ) {
		$find    = array( '/<a rel="button"/', '/<a title=".*?" rel="button"/' );
		$replace = array( '<a rel="button" class="button"', '<a rel="button" class="button"' );

		return preg_replace( $find, $replace, $ulclass, 1 );
	}

	add_filter( 'wp_nav_menu', 'foundationpress_add_menuclass' );
}

?>
