<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

add_action( 'wp_head', 'kb_load_open_graph' );

function kb_load_open_graph() {

    global $post;

    // Standard-Grafik für Seiten ohne Beitragsbild
    $kb_site_logo = get_stylesheet_directory_uri() . '/assets/img/header_bg.jpg';

    // Wenn Startseite
    if ( is_front_page() ) { // Alternativ is_home
        echo '<meta property="og:type" content="website" />';
        echo '<meta property="og:url" content="' . get_bloginfo( 'url' ) . '" />';
        echo '<meta property="og:title" content="' . esc_attr( get_bloginfo( 'name' ) ) . '" />';
        echo '<meta property="og:image" content="' . $kb_site_logo . '" />';
        echo '<meta property="og:description" content="' . esc_attr( get_bloginfo( 'description' ) ) . '" />';
    }

    // Wenn Einzelansicht von Seite, Beitrag oder Custom Post Type
    elseif ( is_singular() ) {
        echo '<meta property="og:type" content="article" />';
        echo '<meta property="og:url" content="' . get_permalink() . '" />';
        echo '<meta property="og:title" content="' . esc_attr( get_the_title() ) . '" />';
        if ( has_post_thumbnail( $post->ID ) ) {
            $kb_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
            echo '<meta property="og:image" content="' . esc_attr( $kb_thumbnail[0] ) . '" />';
        } else
            echo '<meta property="og:image" content="' . $kb_site_logo . '" />';
        echo '<meta property="og:description" content="' . esc_attr( get_the_excerpt() ) . '" />';
    }
}