<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

if ( ! function_exists( 'the_macheete_big_bg' ) ) {
	function the_macheete_big_bg() {

		$bg_position = 'center';
		$url = null;

		if ( get_field( 'eigene_bild_ausrichtung' ) ) {
			$bg_position = get_field( 'eigene_bild_ausrichtung' ) . 'px';
		} elseif(get_field( 'bild_ausrichtung' )) {
			$bg_position = get_field( 'bild_ausrichtung' );
		}

		if ( has_post_thumbnail() ) {
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'big' );
			$url   = $thumb['0'];
			echo '<div class="single-bg grayscale" style="background-image: url(\'' . $url . '\'); background-position-y: ' . $bg_position . '"></div>';
		}
	}
}