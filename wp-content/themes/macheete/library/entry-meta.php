<?php
if ( ! function_exists( 'foundationpress_entry_meta' ) ) :
	function foundationpress_entry_meta() {
		echo '<span class="meta-information">';
		echo '<i class="fa fa-clock-o"></i> <time class="updated" datetime="' . get_the_time( 'c' ) . '">' . get_the_date() . '</time>';
		echo '<span class="seperator"></span>';
		echo '<i class="fa fa-folder-open"></i> ';
		$categories = get_the_category();
		$separator = ' ';
		$output = '';
		if ( ! empty( $categories ) ) {
			foreach( $categories as $category ) {
				$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
			}
			echo trim( $output, $separator );
		}
		echo '</span>';
	}
endif;
?>
