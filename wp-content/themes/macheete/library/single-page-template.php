<?php

function get_custom_cat_template( $single_template ) {
	global $post;

	if ( in_category( 'cases' ) ) {
		$single_template = dirname( __FILE__ ) . '/../single-cases.php';
	}

	return $single_template;
}

add_filter( "single_template", "get_custom_cat_template" );

?>