<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

function addhttp($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$url = "http://" . $url;
	}
	return $url;
}

add_filter("posts_orderby", "my_orderby_filter", 10, 2);
add_filter("posts_orderby", "my_orderbyr_filter", 10, 2);
add_filter("posts_parent", "my_parent_filter", 10, 2);

function my_orderby_filter($orderby, &$query){
	global $wpdb;
	//figure out whether you want to change the order
		if (get_query_var("post_type") == "presse") {
		return "$wpdb->posts.post_title ASC";
	}
	return $orderby;
}

function my_orderbyr_filter($orderby, &$query){
	global $wpdb;
	//figure out whether you want to change the order
	if (get_query_var("post_type") == "referenzen") {
		return "$wpdb->posts.post_title ASC";
	}
	return $orderby;
}

function my_parent_filter($postparent, &$query){
	global $wpdb;
	//figure out whether you want to change the order
	if (get_query_var("post_type") == "presse") {
		return "$wpdb->posts.post_parent = 0";
	}
	return $postparent;
}
