<?php
require get_template_directory() . "/widgets/macheete_leistungen.php";
require get_template_directory() . "/widgets/macheete_contact-info.php";
require get_template_directory() . "/widgets/macheete_post_wall.php";

add_action( 'widgets_init',
	function () {
		register_widget( 'macheete_leistungen' );
		register_widget( 'macheete_contact_info' );
		register_widget( 'macheete_post_wall' );
	} );