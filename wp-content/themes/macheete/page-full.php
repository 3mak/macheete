<?php
/*
Template Name: Full Width
*/
get_header(); ?>
		<?php /* Start loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article <?php post_class( 'no-margin' ) ?> id="post-<?php the_ID(); ?>">

				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<?php comments_template(); ?>
			</article>
		<?php endwhile; // End the loop ?>
<?php get_footer('light'); ?>
