<?php
/**
 * In dieser Datei werden die Grundeinstellungen für WordPress vorgenommen.
 *
 * Zu diesen Einstellungen gehören: MySQL-Zugangsdaten, Tabellenpräfix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es
 * auf der {@link http://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Informationen für die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgeführt,
 * wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von
 * wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden möchtest. */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', ABSPATH . 'wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager


define('DB_NAME', 'd01f1562');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'd01f1562');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', '3RFcEUHztHx3rSnC');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'localhost');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht geändert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden KEY in eine beliebige, möglichst einzigartige Phrase.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle KEYS generieren lassen.
 * Bitte trage für jeden KEY eine eigene Phrase ein. Du kannst die Schlüssel jederzeit wieder ändern,
 * alle angemeldeten Benutzer müssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         '%JQ7K9i7M^s;GCy6]3P)9jn+`(=u.;s1dGF2_1F/m<G6]BJ;RxAjd2sF72=BK3Q1');
define('SECURE_AUTH_KEY',  'WvA9*eEt!j-].Uv_P9K6H@Y^Pu!y*Q-`JKvQ3=H4)So5+grX!?mI^I)0@),HX7*$');
define('LOGGED_IN_KEY',    'a!Tq={(DPtWX^K4^i!*HJ0Vh6`3mt:pV:FJUCOxrFQL&{_cg`j1,wfr0H&z>?lsV');
define('NONCE_KEY',        '5@/Vn*J7Yu;VU{?7gN@a64n[9kxUYn/l1;D|Cm6zdNS;LpP5AnsU$d4;P.o,z{v-');
define('AUTH_SALT',        '9=,Y,@Uml++:yXM<ICpL)%fV2Va.SY9jtCE!aynO%wg|Q7;yW-H^007?dvK:Qc&H');
define('SECURE_AUTH_SALT', '(JHgE98H2}Gb6`]lk&Ly5a:vXd3-H7RN_x8%4MLCMq&aSbO@fy;y[Sr79&A`<r1y');
define('LOGGED_IN_SALT',   'mF}!rKCA>G$o,Q2pLJk+{_@7c`u}S(xBLwB@Ec_b-aS64c(_/pg-P*A6g85<>0[h');
define('NONCE_SALT',       '){*Kaf/lI`$ZE*<Rx%]K-|_q7r0R#i=+gDTJ-b8SD:1gBK;=NufXsGL-;Y]=EwE/');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 *  Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'moe00_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
